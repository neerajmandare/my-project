

package com.student.model.dao;
import java.sql.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultDaoImpl implements ResultDao {
	
	private Connection connection;

	public ResultDaoImpl() {
		connection = ConnectionFactory.getConnection();
	}

	@Override
	public List<Result> getAllResult() {
		List<Result> results=new ArrayList<Result>();
		
//		Student student;
		Result result;
		
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from Result ");
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				result = new Result(rs.getInt("id"),
						rs.getString("fname"), 
						rs.getString("subject"), 
						rs.getString("city"), 
						rs.getString("state"), 
						rs.getBoolean("status"));
				
				
				
				results.add(result);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return results;
	}

}

