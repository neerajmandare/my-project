package com.student.model.dao;
import java.sql.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
	
	private Connection connection;

	public StudentDaoImpl() {
		connection = ConnectionFactory.getConnection();
	}

	@Override
	public List<Student> getAllStudent() {
		List<Student> students=new ArrayList<Student>();
		
		Student student;
		
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from Student ");
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				student = new Student(rs.getInt("id"),
						rs.getString("fname"), 
						rs.getString("subjectname"), 
						rs.getInt("marks"), 
						rs.getInt("testnumber"));
				
				
				
				students.add(student);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return students;
	}

}

