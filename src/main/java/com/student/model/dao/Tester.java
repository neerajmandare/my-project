package com.student.model.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Tester {

	public static void main(String[] args)
	{
		StudentDao sdao = new StudentDaoImpl();
//		ResultDao rdao = new ResultDaoImpl();
		List<Student> studentList = sdao.getAllStudent();
		System.out.println("printing as list");
		studentList.forEach(s->System.out.println(s));
		
//		List<Result> resultList = rdao.getAllResult();
//		System.out.println("printing as list");
//		resultList.forEach(s->System.out.println(s));
		
		Map<Integer, Student> map = new HashMap<>();
		
		Map<Integer, String> mapOfMarkName = studentList.stream().
				collect(Collectors.toMap(Student::getSubjectmarks, Student::getFname));
		
		System.out.println("printing as Map");
		mapOfMarkName.entrySet().stream().sorted(Map.Entry.comparingByValue())
		.forEach(System.out::println);
		
		

		
;	}
}
